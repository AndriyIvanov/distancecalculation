#include "DictanceCalculator.h"
//#include <opencv2/opencv.hpp>
//#include <opencv2/imgproc/imgproc.hpp>

bool Intersection(cv::Point o1, cv::Point p1, cv::Point o2, cv::Point p2, cv::Point &r)
{
    cv::Point x = o2 - o1;
    cv::Point d1 = p1 - o1;
    cv::Point d2 = p2 - o2;

    double cross = d1.x*d2.y - d1.y*d2.x;
    if (abs(cross) < /*EPS*/1e-8)
        return false;

    double t1 = (x.x * d2.y - x.y * d2.x)/cross;
    r = o1 + d1 * t1;
    return true;
}

DictanceCalculator::DictanceCalculator()
{
    _angleWidth = 100;
    _angleHeight = 60;
    _cameraHeight = 1.25;

    _frameHeight = 480;
    _frameWidth = 640;
}

DictanceCalculator::DictanceCalculator(double angleWidth, double angleHeight, double cameraHeight)
{
    _angleWidth = angleWidth;
    _angleHeight = angleHeight;
    _cameraHeight = cameraHeight;
}

void DictanceCalculator::setFrameDimensions(int width, int height)
{
    _frameHeight = height;
    _frameWidth = width;
}

void DictanceCalculator::FramePlaneDistancesCalculation(double inclineAngle)
{
    _distanceToFramePlane = _cameraHeight / tan((0.5 *_angleHeight + inclineAngle) * M_PI/180);
    std::cout << "Distance to frame plane = " << _distanceToFramePlane << std::endl;
    _viewHalfWidth = _distanceToFramePlane * tan(0.5 * _angleWidth * M_PI/180);
    std::cout << "Half width of the frame plane = " << _viewHalfWidth << std::endl;
    _viewHeight = 2 * _cameraHeight;
}

bool DictanceCalculator::CalculateCoords(cv::Point pointPXL, cv::Point2d &pointMtr)
{
    double x, y;
    if (2*pointPXL.y > _frameHeight)
    {
        x = ((2*pointPXL.x - _frameWidth)/(2*pointPXL.y - _frameHeight))*(_viewHalfWidth*_frameHeight/_frameWidth);
        y = _distanceToFramePlane*_frameHeight/(2*pointPXL.y - _frameHeight);
        pointMtr = cv::Point2d(x, y);
        return true;
    }
    return false;
}

bool DictanceCalculator::CalculateDistance(cv::Point pointPXL, double &distance)
{
    std::cout << "_frameHeight = " << _frameHeight << std::endl;
    std::cout << "_frameWidth = " << _frameWidth << std::endl;

    cv::Point2d pointMtr;
    if(this->CalculateCoords(pointPXL, pointMtr))
    {
        distance = sqrt(pointMtr.x*pointMtr.x + pointMtr.y*pointMtr.y);
        return true;
    }
    return false;
}

bool DictanceCalculator::CalculateHeight(cv::Point base, cv::Point up, double &height)
{
    cv::Point center(_frameWidth/2,_frameHeight/2);
    cv::Point baseIntersect, upIntersect;
    if(!Intersection(cv::Point(0,_frameHeight), cv::Point(_frameWidth,_frameHeight), center, base, baseIntersect))
        return false;
    if(!Intersection(cv::Point(baseIntersect.x,_frameHeight), cv::Point(baseIntersect.x,0), center, up, upIntersect))
        return false;
    int heightPxl = baseIntersect.y - upIntersect.y;
    height = _viewHeight * heightPxl / _frameHeight;
    return true;
}





