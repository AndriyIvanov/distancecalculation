QT += core
QT -= gui

TARGET = DistanceCalculation
CONFIG += console
CONFIG -= app_bundle
CONFIG += link_pkgconfig

QMAKE_CXXFLAGS += -std=c++14
DEFINES += QT_DEPRECATED_WARNINGS

TEMPLATE = app

PKGCONFIG += opencv

SOURCES += main.cpp \
    DictanceCalculator.cpp

HEADERS += \
    DictanceCalculator.h

