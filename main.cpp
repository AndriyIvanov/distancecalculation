#include <QCoreApplication>

//#include <opencv2/opencv.hpp>
//#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "DictanceCalculator.h"

using namespace std;
using namespace cv;

DictanceCalculator calc;
cv::Point basePoint, upPoint;

void CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
    if  (event == cv::EVENT_LBUTTONDOWN)
    {
        basePoint = cv::Point(x, y);
        cout << "Left button of the mouse is clicked in position: " << basePoint << endl;

        cv::Point2d target;
        double dist;
        if (calc.CalculateCoords(basePoint, target))
        {
            cout << "Point coordinates in meters: " << target << endl;
            calc.CalculateDistance(cv::Point(x, y), dist);
            cout << "Distance to point in meters = "<< dist << endl;
        }
        else
        {
            cout << "Coordinates and distance can't be found!" << endl;
        }
    }

    if  (event == cv::EVENT_RBUTTONDOWN)
    {
        upPoint = cv::Point(basePoint.x, y);
        cout << "Right button of the mouse is clicked in position: " << cv::Point(x, y) << endl;

        double height;
        if(calc.CalculateHeight(basePoint, upPoint, height))
        {
            cout << "Height of the object in meters = "<< height << endl;
        }
    }
}


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    //VideoCapture video(1); //0,2,4
    VideoCapture video("http://admin:1234qwer@10.11.31.240/ISAPI/Streaming/channels/102/httppreview");
    double angleCorrection = 4.8;

    calc.FramePlaneDistancesCalculation(angleCorrection);

    if(!video.isOpened())
    {
        cout << "Could not read video file" << endl;
        return EXIT_FAILURE;
    }
    cv::Mat frame;
    cv::namedWindow("Image", 1);
    cv::setMouseCallback("Image", CallBackFunc, NULL);

    while(true)
    {
        video.read(frame);
        calc.setFrameDimensions(frame.cols, frame.rows);
        //cout << "frame size (WxH): " << frame.cols << "x" << frame.rows << endl;

        // Display frame.
        imshow("Image", frame);

        // Exit if ESC pressed.
        if(waitKey(1) == 27) break;
    }

    return a.exec();
}


