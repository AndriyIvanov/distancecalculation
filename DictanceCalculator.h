#ifndef DICTANCECALCULATOR_H
#define DICTANCECALCULATOR_H

#include <math.h>
#include <iostream>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>

class DictanceCalculator
{
    double _angleWidth;           //Width aperture angle, degree
    double _angleHeight;          //Height aperture angle, degree
    double _cameraHeight;         //Camera installation height, m

    double _frameHeight;          //Frame height in pixels
    double _frameWidth;           //Frame width in pixels

    double _distanceToFramePlane; //Distance to the frame plane (camera dead zone), m
    double _viewHalfWidth;        //Distance from the center of view to the right/left edge, m
    double _viewHeight;           //Height of the frame plane in m

public:
    DictanceCalculator();
    DictanceCalculator(double angleWidth, double angleHeight, double cameraHeight);

    void setFrameDimensions (int width, int height);

    void FramePlaneDistancesCalculation (double inclineAngle = 0);  //Down '+', Up '-'

    bool CalculateCoords(cv::Point pointPXL, cv::Point2d &pointMtr);                //Return (x,y) coordinates in meters (origin - camera position)
    bool CalculateDistance(cv::Point p, double &distance);                          //Return Euclide distance to point
    bool CalculateHeight(cv::Point base, cv::Point up, double &height);             //return height of the object (placed on the ground) in m
};

#endif // DICTANCECALCULATOR_H
